<?php

namespace Lkt;

/**
 * Class AppVersion
 * @package Lkt\AppVersion
 */
class AppVersion
{
    protected static $APP_VERSION;

    /**
     * @param string $path
     * @return bool
     */
    public static function readFromComposerFile(string $path) :bool
    {
        if (file_exists($path)) {
            $content = json_decode(file_get_contents($path), true);
            if (isset($content['version'])) {
                static::$APP_VERSION = trim($content['version']);
                return true;
            }
        }
        return false;
    }

    public static function get() :string
    {
        return trim(static::$APP_VERSION);
    }

    /**
     * @return int
     */
    public static function getAsNumber() :int
    {
        $arr = explode('.', static::$APP_VERSION);
        if (count($arr) !== 3){
            return 0;
        }
        $major = (int)$arr[0];
        $minor = (int)$arr[1];
        $patch = (int)$arr[2];
        return (int)($major * 10000 + $minor * 100 + $patch);
    }
}